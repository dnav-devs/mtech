#!/usr/bin/env sh
git clone https://gitlab.com/dnav-devs/dnav/ .runner
cd .runner
mkdir publish
dotnet publish -c Release -r linux-x64 --property:PublishDir=$(pwd)/publish/
cd ../
cp publish/dnav .
mkdir -p Mods/mtech/build/
