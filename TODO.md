# mTech Todo

- [ ] Primitive Age
	- [ ] Mechanical Power
		- [ ] Axles/gearboxes/etc.
		- [ ] Waterwheels
		- [ ] Windmills
		- [ ] Handcranks
	- [ ] Aquaducts?
- [ ] "Industrial Revolution"
- [ ] Steam Age
- [ ] LV Age
- [ ] MV Age
- [ ] HV Age
- [ ] Fancy Stuff

## Expansions

Plans to make an "expansion" mod to add fancier space travel and such. Fun stuff.
